template <class Archive>
void openHyperTreeGrid::serialize(Archive &archive)
{
        archive(minBoundingBox);
        archive(maxBoundingBox);
        archive(numberOfHyperTreePerAxis);
        archive(numberOfNodes);

        archive(bitMask);
        archive(scalars);
        archive(scaleArray);

        archive(hyperTrees);
}

