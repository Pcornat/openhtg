#include "openHyperTreeGridCursorIterator.h"
#include "openHyperTreeGridCursor.h"

template<typename cursorType>
treeTraverser<cursorType>::cursorIterator::cursorIterator() {
	isEnd = false;
	traversalMode = ITERATOR_BFS;
}

template<typename cursorType>
treeTraverser<cursorType>::cursorIterator::cursorIterator(bool end) {
	isEnd = end;
}

template<typename cursorType>
void treeTraverser<cursorType>::cursorIterator::setCursor(cursorType &cursorIn) {
	cursor = cursorIn;
}

template<typename cursorType>
cursorType &treeTraverser<cursorType>::cursorIterator::getCursor() {
	return cursor;
}

template<typename cursorType>
treeTraverser<cursorType>::treeTraverser() {
	beginIterator = cursorIterator();
	endIterator = cursorIterator(true);
}

template<typename cursorType>
typename treeTraverser<cursorType>::cursorIterator treeTraverser<cursorType>::begin() {
	return beginIterator;
}

template<typename cursorType>
typename treeTraverser<cursorType>::cursorIterator &treeTraverser<cursorType>::end() {
	return endIterator;
}

template<typename cursorType>
void treeTraverser<cursorType>::setRoot(cursorType &root) {
	beginIterator.setCursor(root);
}

template<typename cursorType>
void treeTraverser<cursorType>::setRoot(cursorType *&root) {
	beginIterator.setCursor(*root);
}

template<typename cursorType>
bool treeTraverser<cursorType>::cursorIterator::operator!=(cursorIterator in) {
	return in.isEnd != isEnd;
}

template<typename cursorType>
typename treeTraverser<cursorType>::cursorIterator &treeTraverser<cursorType>::cursorIterator::operator++() {
	if (traversalMode == ITERATOR_DFS) {
		if (!cursor.isLeaf()) {
			const uint8_t nbNodes = cursor.getNumberOfChildren();
			for (uint8_t i = nbNodes; i > 0; --i) {
				nodeStack.push(cursor);
				cursorType &tmpCursor = nodeStack.top();
				tmpCursor.toChild(i - 1);
			}
		}

		if (!nodeStack.empty()) {
			cursor = nodeStack.top();
			nodeStack.pop();
		} else {
			isEnd = true;
		}
	} else if (traversalMode == ITERATOR_BFS) {
		if (!cursor.isLeaf()) {
			uint32_t nbNodes = cursor.getNumberOfChildren();
			for (std::size_t i = 0; i < nbNodes; ++i) {
				nodeQueue.push(cursor);
				cursorType &tmpCursor = nodeQueue.back();
				tmpCursor.toChild(i);
			}
		}

		if (!nodeQueue.empty()) {
			cursor = nodeQueue.front();
			nodeQueue.pop();
		} else {
			isEnd = true;
		}
	}

	return *this;
}

template<typename cursorType>
void treeTraverser<cursorType>::setTraversalModeToDFS() {
	beginIterator.setTraversalMode(ITERATOR_DFS);
}

template<typename cursorType>
void treeTraverser<cursorType>::setTraversalModeToBFS() {
	beginIterator.setTraversalMode(ITERATOR_BFS);
}

template<typename cursorType>
void treeTraverser<cursorType>::cursorIterator::setTraversalMode(uint8_t mode) {
	traversalMode = mode;
}

template<typename cursorType>
treeTraverser<cursorType>::~treeTraverser() = default;

template<typename cursorType>
treeTraverser<cursorType>::cursorIterator::~cursorIterator() {
	while (!nodeQueue.empty()) {
		nodeQueue.pop();
	}
	while (!nodeStack.empty()) {
		nodeStack.pop();
	}
}

template<typename cursorType>
treeTraverser<cursorType>::cursorIterator::cursorIterator(const cursorIterator &in) {
	*this = in;
}

template<typename cursorType>
typename treeTraverser<cursorType>::cursorIterator &treeTraverser<cursorType>::cursorIterator::operator=(const cursorIterator &in) {
	if (in.isEnd)
		isEnd = true;
	else {

		nodeStack = in.nodeStack;
		nodeQueue = in.nodeQueue;

		cursor = in.cursor;
		isEnd = in.isEnd;

		traversalMode = in.traversalMode;
	}
	return *this;
}