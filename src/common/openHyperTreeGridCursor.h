//
// Created by antoine on 25/05/2020.
//

#ifndef SERIALIZED_HTG_SERIALIZEDHYPERTREEGRIDCURSOR_H
#define SERIALIZED_HTG_SERIALIZEDHYPERTREEGRIDCURSOR_H

#include "CurrentNodeManager.h"
#include "HistoryManager.h"
#include "HistoryManagerDummy.h"
#include "GeometryManager.h"
#include "GeometryManagerDummy.h"

#include <map>
#include <stack>
#include <memory>

#include "openHyperTreeGrid.h"
#include "openHyperTree.h"

typedef struct {
	uint32_t index;
	std::array<double, 3> minBoundingBox;
} geometry_node_t;

typedef struct {
	uint32_t index;
} node_t;

template<class HistoryType, class GeometryType = GeometryManagerDummy>
class openHyperTreeGridCursor {

public:
	typedef typename HistoryType::NodeType CursorNodeType;

	///Constructors
	explicit openHyperTreeGridCursor(openHyperTreeGrid &htgIn) : hyperTreeGrid(&htgIn) {
		currentHyperTree = hyperTreeGrid->getBeginTreeIterator();
		initRoot();
	}

	openHyperTreeGridCursor() = default;

	openHyperTreeGridCursor(const openHyperTreeGridCursor &cursorIn) {
		hyperTreeGrid = cursorIn.hyperTreeGrid;
		currentHyperTree = cursorIn.currentHyperTree;
		currentNodeManager.setCurrentNode(cursorIn.currentNodeManager.getCurrentNode());
		currentNodeManager.setLevel(cursorIn.currentNodeManager.getLevel());
		historyManager.copyHistoryStack(cursorIn.historyManager);
	}

	void initRoot() {
		historyManager.clearHistory();
		if (currentHyperTree != hyperTreeGrid->getEndTreeIterator()) {
			double bounds[6];
			getTreeBounds(bounds);
			geometryManager.setCurrentNodeMinBoundingBox(currentNodeManager.getCurrentNode(), bounds[0], bounds[2], bounds[4]);
			currentNodeManager.setCurrentNodeIndex(0);
			currentNodeManager.resetLevel();

			if (currentHyperTree->second->getNumberOfNodes() == 0) {
				addNode(1u);
			}
			hyperTreeGrid->setScale(0);
		}
	}

	///Tree navigation
	bool toTree(uint32_t treeIndex, bool create = false) {
		currentHyperTree = hyperTreeGrid->getTree(treeIndex, create);
		initRoot();

		return currentHyperTree != hyperTreeGrid->getEndTreeIterator();
	}

	bool toNextTree() {

		++currentHyperTree;
		initRoot();
		return currentHyperTree != hyperTreeGrid->getEndTreeIterator();


	}

	///Tree traversal
	void toParent() {
		historyManager.toParent(currentNodeManager);
	}

	void toChild(uint32_t childIndex) {
		const uint8_t nbChildren = getNumberOfChildren();
		if (childIndex >= nbChildren) {
			std::cerr << "Child Index out of number of children" << std::endl;
			exit(EXIT_FAILURE);
		}

		historyManager.addElement(currentNodeManager.getCurrentNode());

		if (isLeaf()) {
			std::cerr << "To Child in a leaf node" << std::endl;
			exit(EXIT_FAILURE);
		}
		const uint32_t newIndex = currentHyperTree->second->getElderChildIndexValue(
				currentNodeManager.getCurrentNodeIndex()) + childIndex;
		currentNodeManager.setCurrentNodeIndex(newIndex);

		const double *scale = hyperTreeGrid->getScale(currentNodeManager.getLevel());
		geometryManager.moveCurrentNodeMinBoundingBox(currentNodeManager.getCurrentNode(), childIndex, scale);
		currentNodeManager.incrementLevel();
	}
	//void toRoot();

	///Getters
	uint32_t getGlobalIndex() const {
		return currentHyperTree->second->getScalarOffset() +
			   currentNodeManager.getCurrentNodeIndex();
	}///Node index in the entire HyperTreeGrid
	uint32_t getNodeIndex() const {
		return currentNodeManager.getCurrentNodeIndex();
	}///Node index in the current HyperTree
	uint32_t getTreeIndex() const {
		return currentHyperTree->first;
	}

	uint32_t getLevel() const {
		return currentNodeManager.getLevel();
	}

	void getBounds(double bounds[6]) const {
		const double *scale = hyperTreeGrid->getScale(currentNodeManager.getLevel());
		geometryManager.computeBounds(currentNodeManager.getCurrentNode(), scale, bounds);
	}

	void getTreeBounds(double bounds[6]) const {
		uint32_t treeIndex = currentHyperTree->second->getTreeIndex();
		std::array<uint32_t, 3> nbTreesPerAxis{};
		hyperTreeGrid->getDimensions(nbTreesPerAxis.data());
		std::array<double, 3> hyperTreeSize{ 0 };
		std::array<uint32_t, 3> hyperTreePos{ 0 };
		hyperTreeGrid->getHyperTreeSize(hyperTreeSize.data());
		uint32_t XxY = (nbTreesPerAxis[0] * nbTreesPerAxis[1]);
		hyperTreePos[2] = treeIndex / XxY;
		treeIndex -= hyperTreePos[2] * XxY;
		hyperTreePos[1] = treeIndex / nbTreesPerAxis[0];
		hyperTreePos[0] = treeIndex - hyperTreePos[1] * nbTreesPerAxis[0];

		bounds[0] = hyperTreePos[0] * hyperTreeSize[0];
		bounds[1] = (hyperTreePos[0] + 1) * hyperTreeSize[0];

		bounds[2] = hyperTreePos[1] * hyperTreeSize[1];
		bounds[3] = (hyperTreePos[1] + 1) * hyperTreeSize[1];

		bounds[4] = hyperTreePos[2] * hyperTreeSize[2];
		bounds[5] = (hyperTreePos[2] + 1) * hyperTreeSize[2];
	}

	uint32_t getNumberOfChildren() const {
		return NUMBER_OF_CHILDREN;
	}

	openHyperTree *getHyperTree() {
		return currentHyperTree->second.get();
	}

	bool isLeaf() const {
		return currentHyperTree->second->getElderChildIndexValue(
				currentNodeManager.getCurrentNodeIndex()) == UINT32_MAX;
	}

	bool isMasked() const {
		const uint32_t id = currentNodeManager.getCurrentNodeIndex() + currentHyperTree->second->getScalarOffset();
		return hyperTreeGrid->getBitMaskValue(id);
	}
	//bool exists();

	///Setters
	void setMask(bool masked) {
		uint32_t id = getGlobalIndex();
		hyperTreeGrid->setBitMaskValue(id, masked);
	}

	void setScalarValue(const std::string &field, float value) {
		hyperTreeGrid->setScalarValue(field, getGlobalIndex(), value);
	}

	void subdivideLeaf() {
		if (!isLeaf()) {
			return;
		}

		addNode(getNumberOfChildren());
		hyperTreeGrid->setScale(currentNodeManager.getLevel() + 1);
	}

	void addNode(uint32_t numberOfNodes) {
		auto hyperTree = currentHyperTree->second;
		hyperTree->addNodes(numberOfNodes,
							currentNodeManager.getCurrentNodeIndex());
		hyperTreeGrid->addNodes(numberOfNodes);
	}


private:
	openHyperTreeGrid *hyperTreeGrid{};
	std::map<uint32_t, std::shared_ptr<openHyperTree>>::iterator currentHyperTree;

	//node_t currentNode;
	//bool isOriented;
	//uint32_t level;
	//std::stack<node_t> nodeStack;
	CurrentNodeManager<CursorNodeType> currentNodeManager;
	HistoryType historyManager;
	GeometryType geometryManager;
};

class openHyperTreeGridCursorOriented : public openHyperTreeGridCursor<HistoryManagerDummy<node_t> > {
public:
	openHyperTreeGridCursorOriented() : openHyperTreeGridCursor() {}

	explicit openHyperTreeGridCursorOriented(openHyperTreeGrid &ohtg) : openHyperTreeGridCursor(ohtg) {}
};

class openHyperTreeGridCursorOrientedGeometry : public openHyperTreeGridCursor<HistoryManagerDummy<geometry_node_t>, GeometryManager> {
public:
	openHyperTreeGridCursorOrientedGeometry() : openHyperTreeGridCursor() {}

	explicit openHyperTreeGridCursorOrientedGeometry(openHyperTreeGrid &ohtg) : openHyperTreeGridCursor(ohtg) {}
};

class openHyperTreeGridCursorNonOriented : public openHyperTreeGridCursor<HistoryManager<node_t> > {
public:
	openHyperTreeGridCursorNonOriented() : openHyperTreeGridCursor() {}

	explicit openHyperTreeGridCursorNonOriented(openHyperTreeGrid &ohtg) : openHyperTreeGridCursor(ohtg) {}
};

class openHyperTreeGridCursorNonOrientedGeometry : public openHyperTreeGridCursor<HistoryManager<geometry_node_t>, GeometryManager> {
public:
	openHyperTreeGridCursorNonOrientedGeometry() : openHyperTreeGridCursor() {}

	explicit openHyperTreeGridCursorNonOrientedGeometry(openHyperTreeGrid &ohtg) : openHyperTreeGridCursor(ohtg) {}
};

#endif //SERIALIZED_HTG_SERIALIZEDHYPERTREEGRIDCURSOR_H
