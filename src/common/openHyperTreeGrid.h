//
// Created by antoine on 25/05/2020.
//

#ifndef SERIALIZED_HTG_SERIALIZEDHYPERTREEGRID_H
#define SERIALIZED_HTG_SERIALIZEDHYPERTREEGRID_H

#include <cstdint>
#include <vector>
#include <map>
#include <memory>
#include <any>
#include <string>

constexpr uint8_t NUMBER_OF_CHILDREN = 8; ///We only handle 8 children per node for now

class openHyperTree;

class openHyperTreeGrid {

public:
	openHyperTreeGrid() = default;

	//bool insertTree(std::shared_ptr<openHyperTree>, uint32_t);

	std::map<uint32_t, std::shared_ptr<openHyperTree>>::iterator getTree(uint32_t, bool create = false);

	[[nodiscard]] uint32_t getNumberOfNodes() const;

	void setBoundingBox(const double[3], const double[3]);

	void getHyperTreeSize(double[3]);

	void getDimensions(uint32_t [3]);

	void setDimensions(uint32_t [3]);

	void setDimensions(uint32_t, uint32_t, uint32_t);

	void setBitMaskValue(uint32_t, bool);

	bool getBitMaskValue(uint32_t);

	void setScalarValue(const std::string &, uint32_t, float);

	//float getScalarValue(const std::string &, uint32_t);

	uint32_t getNumberOfLevels();

	const double *getScale(uint32_t level);

	void setScale(uint32_t level);

	void addScalarField(const std::string &);

	std::map<uint32_t, std::shared_ptr<openHyperTree>>::iterator getBeginTreeIterator();

	std::map<uint32_t, std::shared_ptr<openHyperTree>>::iterator getEndTreeIterator();

	void addNodes(uint32_t);

	uint32_t getMaxNumberOfTrees();

	void getBounds(double bounds[6]);

	std::vector<float> &getscalarArray(const std::string &);

	std::vector<uint8_t> &getBitMask();

	bool isEqualTo(openHyperTreeGrid &);

	bool hasScalarField(const char *scalarName) const {
		return scalars.count(scalarName);
	}

	[[nodiscard]] std::vector<std::string> getscalarArrayNames() const {
		std::vector<std::string> ret;
		for (const auto &elements : this->scalars) {
			ret.push_back(elements.first);
		}
		return ret;
	}

	template<typename Archive>
	void serialize(Archive &archive);

private:
//	friend class cereal::access;

	std::array<uint32_t, 3> numberOfHyperTreePerAxis{ 0, 0, 0 };

	std::array<double, 3> minBoundingBox{};
	std::array<double, 3> maxBoundingBox{};

	uint64_t numberOfNodes{ 0 };

	std::map<uint32_t, std::shared_ptr<openHyperTree> > hyperTrees;

	std::map<std::string, std::vector<float>> scalars;
	std::vector<uint8_t> bitMask;

	std::vector<std::array<double, 3>> scaleArray;

};

#include "openHyperTreeGrid.txx"

#endif //SERIALIZED_HTG_SERIALIZEDHYPERTREEGRID_H
