//
// Created by antoine on 25/05/2020.
//

#include "openHyperTree.h"

openHyperTree::openHyperTree() {
	treeIndex = 0;
	numberOfNodes = 0;
	scalarOffset = 0;
}


openHyperTree::openHyperTree(const uint32_t index) {
	treeIndex = index;
	numberOfNodes = 0;
	scalarOffset = 0;
}

openHyperTree::openHyperTree(const openHyperTree &hyperTreeIn) {
	treeIndex = hyperTreeIn.treeIndex;
	numberOfNodes = hyperTreeIn.numberOfNodes;
	scalarOffset = hyperTreeIn.scalarOffset;
	elderChildIndex = hyperTreeIn.elderChildIndex;
}

uint32_t openHyperTree::getNumberOfNodes() const {
	return numberOfNodes;
}

uint32_t openHyperTree::getTreeIndex() const {
	return treeIndex;
}

uint64_t openHyperTree::getScalarOffset() const {
	return scalarOffset;
}

std::vector<uint32_t> &openHyperTree::getElderChildIndex() {
	return elderChildIndex;
}

uint32_t openHyperTree::getElderChildIndexValue(uint32_t index) {
	if (index >= elderChildIndex.size())
		return UINT32_MAX;
	return elderChildIndex[index];
}

void openHyperTree::setTreeIndex(uint32_t index) {
	treeIndex = index;
}

void openHyperTree::setScalarOffset(uint32_t offSet) {
	scalarOffset = offSet;
}

void openHyperTree::setNumberOfNodes(uint32_t nbNodes) {
	numberOfNodes = nbNodes;
}

void openHyperTree::setElderChildIndex(const std::vector<uint32_t> &elderChild) {
	elderChildIndex = elderChild;
}

void openHyperTree::addNodes(uint32_t nbNewNodes, uint32_t newParentIndex) {
	if (newParentIndex >= elderChildIndex.size()) {
		const uint32_t newSize = newParentIndex + 1;
		elderChildIndex.resize(newSize, UINT32_MAX);
	}
	if (nbNewNodes > 1)
		elderChildIndex[newParentIndex] = numberOfNodes;
	numberOfNodes += nbNewNodes;
}
