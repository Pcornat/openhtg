//
// Created by duboisj on 18/06/2020.
//

#ifndef OPENHTG_GEOMETRYMANAGER_H
#define OPENHTG_GEOMETRYMANAGER_H

#include <cstdint>
#include <array>

class GeometryManager {
public:
	template<class NodeType>
	void setCurrentNodeMinBoundingBox(NodeType &node, double minBoundX, double minBoundY, double minBoundZ) {
		std::array<double, 3> &minBoundingBox = node.minBoundingBox;
		minBoundingBox = { minBoundX, minBoundY, minBoundZ };
	}

	template<class NodeType>
	void moveCurrentNodeMinBoundingBox(NodeType &node, uint32_t childIndex, const double *scale) {
		std::array<double, 3> &minBoundingBox = node.minBoundingBox;
		minBoundingBox[0] += (childIndex & 1u) * (scale[0] * 0.5f);
		minBoundingBox[1] += ((childIndex & 2u) >> 1u) * (scale[1] * 0.5f);
		minBoundingBox[2] += ((childIndex & 4u) >> 2u) * (scale[2] * 0.5f);
	}

	template<class NodeType>
	void computeBounds(const NodeType &node, const double *scale, double bounds[6]) const {
		const std::array<double, 3> &minBoundingBox = node.minBoundingBox;
		bounds[0] = minBoundingBox[0];
		bounds[1] = minBoundingBox[0] + scale[0];
		bounds[2] = minBoundingBox[1];
		bounds[3] = minBoundingBox[1] + scale[1];
		bounds[4] = minBoundingBox[2];
		bounds[5] = minBoundingBox[2] + scale[2];
	}
};

#endif // OPENHTG_GEOMETRYMANAGER_H
