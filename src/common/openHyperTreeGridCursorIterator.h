//
// Created by antoine on 11/06/2020.
//

#ifndef OPENHTG_OPENHYPERTREEGRIDCURSORITERATOR_H
#define OPENHTG_OPENHYPERTREEGRIDCURSORITERATOR_H

#include <cstdint>
#include <stack>
#include <queue>

const uint8_t ITERATOR_DFS = 0;

const uint8_t ITERATOR_BFS = 1;

template<class cursorType>
class treeTraverser {
public:
	class cursorIterator {
	public:
		cursorIterator();

		explicit cursorIterator(bool end);

		cursorIterator(const cursorIterator &);

		~cursorIterator();

		void setCursor(cursorType &cursorIn);

		cursorIterator &operator++();

		cursorType &getCursor();

		bool operator!=(cursorIterator in);

		void setTraversalMode(uint8_t);

		cursorIterator &operator=(const cursorIterator &);

	private:
		cursorType cursor;

		std::stack<cursorType> nodeStack;
		std::queue<cursorType> nodeQueue;

		uint8_t traversalMode{};

		bool isEnd{};
	};

private:
	cursorIterator beginIterator;
	cursorIterator endIterator;

public:
	treeTraverser();

	[[maybe_unused]] treeTraverser(const treeTraverser &);

	~treeTraverser();

	cursorIterator begin();

	cursorIterator &end();

	void setRoot(cursorType &root);

	void setRoot(cursorType *&root);

	void setTraversalModeToDFS();

	void setTraversalModeToBFS();
};

#include "openHyperTreeGridCursorIterator.txx"

#endif //OPENHTG_OPENHYPERTREEGRIDCURSORITERATOR_H
