//
// Created by antoine on 15/06/2020.
//

#include <bitset>
#include <iostream>
#include <map>

#include "openHyperTreeGrid.h"
#include "openHyperTreeGridCursor.h"
#include "openHyperTree.h"

void showHyperTreeGridInfos(openHyperTreeGrid htg)
{
	double bounds[6];
	htg.getBounds(bounds);
	std::cout << "min bouding box : " << bounds[0] << " " << bounds[2] << " " << bounds[4] << std::endl;
	std::cout << "max bouding box : " << bounds[1] << " " << bounds[3] << " " << bounds[5] << std::endl;

	uint32_t dim[3];
	htg.getDimensions(dim);
	std::cout << "HyperTrees per axis : " << dim[0] << " " << dim[1] << " " << dim[2] << std::endl;

	std::cout << "Number of nodes : " << htg.getNumberOfNodes() << std::endl;

	std::vector<uint8_t> bitMask = htg.getBitMask();
	std::cout << std::endl << "BitMask :" << std::endl;
	for (uint8_t mask : bitMask)
	{
		const std::string bm = std::bitset<8>(mask).to_string();
		std::cout << bm;
	}
	std::cout << std::endl;

	std::vector<float> scalarArray = htg.getscalarArray("scalar");
	std::cout << std::endl << "Scalar array :" << std::endl;
	for (float scalar : scalarArray)
	{
		std::cout << scalar << " ";
	}
	std::cout << std::endl;

	auto treeIterator = htg.getBeginTreeIterator();

	for (treeIterator; treeIterator != htg.getEndTreeIterator(); ++treeIterator)
	{
		std::cout << std::endl << "Tree " << treeIterator->first << std::endl;

		std::cout << "Number of nodes : " << treeIterator->second->getNumberOfNodes() << std::endl;

		std::cout << std::endl << "Elder child index :" << std::endl;
		std::vector<uint32_t> elderChildIndex = treeIterator->second->getElderChildIndex();
		for (uint32_t index : elderChildIndex)
		{
			std::cout << index << " ";
		}
		std::cout << std::endl;
	}

}

int main(int argc, char *argv[])
{
	openHyperTreeGrid htg;

	htg.setDimensions(1, 1, 1);

	double minBounds[3] = {0, 0, 0};
	double maxBounds[3] = {10, 10, 10};
	htg.setBoundingBox(minBounds, maxBounds);

	htg.addScalarField("scalar");

	openHyperTreeGridCursorNonOriented cursor(htg);

	std::cout << "Empty HTG : " << std::endl;
	showHyperTreeGridInfos(htg);

	cursor.toTree(0, true);
	std::cout << "HTG with one 1-leveled tree" << std::endl;
	showHyperTreeGridInfos(htg);

	cursor.setMask(false);
	cursor.subdivideLeaf();

	for (uint32_t i = 0; i < cursor.getNumberOfChildren(); ++i)
	{
		if (i % 2)
		{
			cursor.toChild(i);

			cursor.setMask(false);
			cursor.setScalarValue("scalar", i * 2.f);
			cursor.subdivideLeaf();

			cursor.toParent();
		}
	}

	std::cout << "HTG with subdivided nodes :" << std::endl;
	showHyperTreeGridInfos(htg);

	for(uint32_t i = 0; i < 3; ++i)
	{
		cursor.subdivideLeaf();
		cursor.toChild(0);
		cursor.setMask(false);
		cursor.setScalarValue("scalar", i + 1);
	}

	std::cout << "HTG with more levels :" << std::endl;
	showHyperTreeGridInfos(htg);

	return 0;
}