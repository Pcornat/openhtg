//
// Created by antoine on 15/06/2020.
//

#include <openHyperTreeGridCursor.h>
#include <iostream>
#include "openHyperTreeGrid.h"
#include "openHyperTreeGridCerealSerialization.h"

int main(int argc, char *argv[])
{
	openHyperTreeGrid htg;

	htg.setDimensions(2, 2, 2);

	double minBounds[3] = {0, 0, 0};
	double maxBounds[3] = {10, 10, 10};
	htg.setBoundingBox(minBounds, maxBounds);

	auto cursor = openHyperTreeGridCursorNonOriented(htg);

	htg.addScalarField("scalar");

	for (uint32_t i = 0; i < htg.getMaxNumberOfTrees(); ++i)
	{
		cursor.toTree(i, true);
		if (cursor.getTreeIndex() % 2)
		{
			cursor.setMask(false);
			cursor.subdivideLeaf();
			cursor.setScalarValue("scalar", i);
			for (uint32_t j = 0; j < cursor.getNumberOfChildren(); ++j)
			{
				if (j % 2)
				{
					cursor.toChild(j);
					cursor.setMask(false);
					cursor.setScalarValue("scalar", i * j);
					cursor.toParent();
				}
			}
		}
	}

	const std::string filename = "exampleHTG.shtg";

	openhtg::io::mcereal::writeInFile(filename, htg);

	openHyperTreeGrid htg2;
	openhtg::io::mcereal::loadFromFile(filename, htg2);

	if (htg.isEqualTo(htg2))
	{
		std::cout << "Both HTGs are equal !" << std::endl;
	}
	else
	{
		std::cerr << "Error : HTGs should be equal" << std::endl;
	}
	return 0;

}